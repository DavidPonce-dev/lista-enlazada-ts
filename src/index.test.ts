import { Nodo } from "./clases/Nodo";
import { ListaEnlazada } from "./clases/ListaEnlazada";

describe("prueba de nodos", () => {
  const lista: ListaEnlazada = new ListaEnlazada();

  test("agregar nodos", () => {
    lista.insertar(1);
    lista.insertar(2);
    lista.insertar(3);
    expect(lista.toString()).toBe("1,2,3");
  });

  test("remover nodos", () => {
    expect(lista.remover(2)).toBe(true);
    expect(lista.remover(4)).toBe(false);
  })

  test("obtener nodo", () => {
    expect(lista.getNode(1).toString()).toBe("2");
    expect(lista.getNode(2).toString()).toBe("2");
    expect(lista.getNode(4).toString()).toBe("2");

  })

  test("obtener nodos", () => {
    expect(lista.toString()).toBe("1,2");
  })
});
