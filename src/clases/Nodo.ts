export class Nodo {
  private valor!: number;
  private siguiente!: Nodo;

  public getValor(): number {
    return this.valor;
  }

  public setValor(valor: number) {
    this.valor = valor;
  }

  public getSiguiente(): Nodo {
    return this.siguiente;
  }

  public setSiguiente(siguiente: Nodo) {
    this.siguiente = siguiente;
  }
  
  public hasNext(): boolean {
    return !!this.getSiguiente();
  }

  public toString(): string {
    return this.getValor().toString();
  }
}
