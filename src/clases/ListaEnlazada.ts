import { Nodo } from "./Nodo";

export class ListaEnlazada {
  private head!: Nodo;
  private lenght: number = 0;

  public insertar(dato: number) {
    if (!this.head) {
      this.head = new Nodo();
      this.head.setValor(dato);
      this.lenght++;
      return;
    }
    let lastNode: Nodo = this.head;
    while (lastNode.hasNext()) {
      console.log(lastNode);
      lastNode = lastNode.getSiguiente();
    }
    lastNode.setSiguiente(new Nodo());
    lastNode.getSiguiente().setValor(dato);
    this.lenght++;
  }

  public remover(indice: number) {
    if (indice > this.lenght) return false;
    let lastNode: Nodo = this.head;
    for (let i = 0; i < indice - 1; i++) {
      lastNode = lastNode.getSiguiente();
    }
    lastNode.setSiguiente(lastNode.getSiguiente().getSiguiente());
    this.lenght--;
    return true;
  }

  public getNode (indice: number) {
    if(indice-1 > this.lenght) return false
    let lastNode: Nodo = this.head;
    for (let i = 0; i < indice; i++) {
      lastNode = lastNode.getSiguiente();
    }
    return lastNode;
  }

  public toString() {
    let lastNode: Nodo = this.head;
    const nodestr: string[] = [];
    for (let i = 0; i < this.lenght; i++) {
      nodestr.push(lastNode.toString());
      lastNode = lastNode.getSiguiente();
    }

    return nodestr.toString();
  }
}
