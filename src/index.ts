import { ListaEnlazada } from "./clases/ListaEnlazada";

const lista = new ListaEnlazada();
lista.insertar(1);
lista.insertar(2);
lista.insertar(3);

console.log(lista.toString());

lista.insertar(4);
lista.remover(2);

console.log(lista.toString());
